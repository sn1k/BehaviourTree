
#include <chrono>
#include <iostream>
#include <thread>

#include "Tree/BehaviourTree.h"

int main()
{
	std::string testTree = "";
	BehaviourTree tree(testTree);

	while (true)
	{	
		tree.Process();
		std::cout << tree.Process() << std::endl << std::flush;
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}

    return 0;
}

