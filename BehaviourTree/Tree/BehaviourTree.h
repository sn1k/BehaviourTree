#pragma once

#include <string>

#include "Nodes/BaseNodes/BehaviourTreeNode.h"

class BehaviourTree
{
public:
	BehaviourTree() = delete;
	BehaviourTree(const std::string& in_treeJSON);
	~BehaviourTree();

	void BuildTree(const std::string& in_treeJSON);

	BehaviourTreeNode::RETURN_TYPE Process();

private:
	BehaviourTreeNode* m_root = nullptr;
};

