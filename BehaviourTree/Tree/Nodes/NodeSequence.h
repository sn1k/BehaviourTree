#pragma once

#include "BaseNodes/NodeComposite.h"

class NodeSequence : public NodeComposite
{
public:
	NodeSequence();
	~NodeSequence();

	void Init() override;
	BehaviourTreeNode::RETURN_TYPE Process() override;
};

