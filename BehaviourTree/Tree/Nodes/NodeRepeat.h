#pragma once

#include "BaseNodes/NodeDecorator.h"

class NodeRepeat : public NodeDecorator
{
public:
	NodeRepeat();
	~NodeRepeat();

	BehaviourTreeNode::RETURN_TYPE Process() override;
};

