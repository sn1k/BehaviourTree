#include "NodeRepeatUntilFail.h"


NodeRepeatUntilFail::NodeRepeatUntilFail()
{
}


NodeRepeatUntilFail::~NodeRepeatUntilFail()
{
}

BehaviourTreeNode::RETURN_TYPE NodeRepeatUntilFail::Process()
{
	if (m_child == nullptr)
		return BehaviourTreeNode::RETURN_TYPE::FAILURE;

	BehaviourTreeNode::RETURN_TYPE childReturn = m_child->Process();

	if (childReturn == BehaviourTreeNode::RETURN_TYPE::FAILURE)
		return BehaviourTreeNode::RETURN_TYPE::FAILURE;

	return BehaviourTreeNode::RETURN_TYPE::PROCESSING;
}