#pragma once
#include "..\BaseNodes\BehaviourTreeNode.h"

class LeafUnlockDoor : public BehaviourTreeNode
{
public:
	LeafUnlockDoor();
	~LeafUnlockDoor();

	RETURN_TYPE Process() override;

	void Init() override;

};

