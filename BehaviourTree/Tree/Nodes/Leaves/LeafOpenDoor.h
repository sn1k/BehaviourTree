#pragma once
#include "..\BaseNodes\BehaviourTreeNode.h"

class LeafOpenDoor : public BehaviourTreeNode
{
public:
	LeafOpenDoor();
	~LeafOpenDoor();

	RETURN_TYPE Process() override;

	void Init() override;
};

