#pragma once
#include "..\BaseNodes\BehaviourTreeNode.h"

class LeafWalkToDoor : public BehaviourTreeNode
{
public:
	LeafWalkToDoor();
	~LeafWalkToDoor();

	RETURN_TYPE Process() override;

	void Init() override;
};

