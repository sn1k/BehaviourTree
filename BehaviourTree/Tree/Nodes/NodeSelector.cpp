#include "NodeSelector.h"


NodeSelector::NodeSelector()
{
}


NodeSelector::~NodeSelector()
{
}


void NodeSelector::Init()
{
	m_currentChildNode = m_childNodes.begin();
}


BehaviourTreeNode::RETURN_TYPE NodeSelector::Process()
{
	if (m_currentChildNode == m_childNodes.end())
		return BehaviourTreeNode::RETURN_TYPE::FAILURE;

	while (m_currentChildNode != m_childNodes.end())
	{
		BehaviourTreeNode::RETURN_TYPE childReturn = (*m_currentChildNode)->Process();

		if (childReturn == BehaviourTreeNode::RETURN_TYPE::PROCESSING)
			return BehaviourTreeNode::RETURN_TYPE::PROCESSING;

		if (childReturn == BehaviourTreeNode::RETURN_TYPE::SUCCESS)
			return BehaviourTreeNode::RETURN_TYPE::SUCCESS;

		++m_currentChildNode;
	}

	return BehaviourTreeNode::RETURN_TYPE::FAILURE;
}
