#pragma once

#include "BaseNodes/NodeComposite.h"

class NodeSelector : public NodeComposite
{
public:
	NodeSelector();
	~NodeSelector();

	void Init() override;
	BehaviourTreeNode::RETURN_TYPE Process() override;
};

