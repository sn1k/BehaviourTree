#include "NodeSequence.h"


NodeSequence::NodeSequence()
{
}


NodeSequence::~NodeSequence()
{
}

void NodeSequence::Init()
{
	m_currentChildNode = m_childNodes.begin();
}

BehaviourTreeNode::RETURN_TYPE NodeSequence::Process()
{	
	if (m_currentChildNode == m_childNodes.end())
		return BehaviourTreeNode::RETURN_TYPE::FAILURE;

	while (m_currentChildNode != m_childNodes.end())
	{
		BehaviourTreeNode::RETURN_TYPE childReturn = (*m_currentChildNode)->Process();

		if (childReturn == BehaviourTreeNode::RETURN_TYPE::PROCESSING)
			return BehaviourTreeNode::RETURN_TYPE::PROCESSING;

		if (childReturn == BehaviourTreeNode::RETURN_TYPE::FAILURE)
			return BehaviourTreeNode::RETURN_TYPE::FAILURE;
		
		++m_currentChildNode;
	}

	return BehaviourTreeNode::RETURN_TYPE::SUCCESS;
}