#include "NodeRepeat.h"


NodeRepeat::NodeRepeat()
{
}


NodeRepeat::~NodeRepeat()
{
}



BehaviourTreeNode::RETURN_TYPE NodeRepeat::Process()
{
	if (m_child == nullptr)
		return BehaviourTreeNode::RETURN_TYPE::FAILURE;

	//while (true)
	{
		BehaviourTreeNode::RETURN_TYPE childReturn = m_child->Process();

		//if (childReturn == BehaviourTreeNode::RETURN_TYPE::PROCESSING)
			return BehaviourTreeNode::RETURN_TYPE::PROCESSING;
	}
}
