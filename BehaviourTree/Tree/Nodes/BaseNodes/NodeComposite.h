#pragma once

#include "BehaviourTreeNode.h"


class NodeComposite : public BehaviourTreeNode
{
public:
	typedef std::vector<BehaviourTreeNode*> ChildNodeVector;

public:
	NodeComposite();
	~NodeComposite();

protected:
	
	ChildNodeVector m_childNodes = ChildNodeVector();
	ChildNodeVector::iterator m_currentChildNode;
};

