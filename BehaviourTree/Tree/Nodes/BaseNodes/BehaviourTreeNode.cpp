
#include "BehaviourTreeNode.h"


BehaviourTreeNode::BehaviourTreeNode()
{
}


BehaviourTreeNode::~BehaviourTreeNode()
{
}


std::ostream& operator<<(std::ostream& os, BehaviourTreeNode::RETURN_TYPE in_ret)
{
	typedef BehaviourTreeNode::RETURN_TYPE retType;

	switch (in_ret)
	{
		case retType::FAILURE: os << "[failure]";    break;
		case retType::PROCESSING: os << "[processing]"; break;
		case retType::SUCCESS: os << "[success]"; break;
		default: os.setstate(std::ios_base::failbit);
	}
	return os;
}