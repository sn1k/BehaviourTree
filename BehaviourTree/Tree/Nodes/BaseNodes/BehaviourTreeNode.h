#pragma once

#include <ostream>
#include <vector>


class BehaviourTreeNode
{
public:
	enum class RETURN_TYPE { SUCCESS, FAILURE, PROCESSING };


public:
	BehaviourTreeNode();
	~BehaviourTreeNode();

	virtual RETURN_TYPE Process() = 0;

	virtual void Init() {}

	// maybe can do this on return
	//void Reset(); // reset internal pointer so we can retraverse this node

protected:
};

std::ostream& operator<<(std::ostream& os, BehaviourTreeNode::RETURN_TYPE in_ret);
