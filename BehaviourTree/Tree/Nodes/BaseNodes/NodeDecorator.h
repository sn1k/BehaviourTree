#pragma once
#include "BehaviourTreeNode.h"

class NodeDecorator : public BehaviourTreeNode
{
public:
	NodeDecorator();
	~NodeDecorator();

protected:
	BehaviourTreeNode* m_child = nullptr;
};

