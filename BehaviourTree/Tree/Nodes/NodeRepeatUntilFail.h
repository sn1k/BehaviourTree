#pragma once

#include "BaseNodes/NodeDecorator.h"

class NodeRepeatUntilFail : public NodeDecorator
{
public:
	NodeRepeatUntilFail();
	~NodeRepeatUntilFail();

	BehaviourTreeNode::RETURN_TYPE Process() override;
};
