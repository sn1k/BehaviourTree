
#include "BehaviourTree.h"

#include "..\include\json\json.h"

// included only for compile test
#include "Nodes/Leaves/LeafWalkToDoor.h"


BehaviourTree::BehaviourTree(const std::string& in_treeJSON)
{
	BuildTree(in_treeJSON);
}


BehaviourTree::~BehaviourTree()
{
}

void BehaviourTree::BuildTree(const std::string& in_treeJSON)
{
	Json::Reader reader;
	
	Json::Value root;
	bool ok = reader.parse(in_treeJSON, root);

	if (root[""].asString().empty())
	{
	}

	if (m_root == nullptr)
	{
		m_root = new LeafWalkToDoor();
	}
}

BehaviourTreeNode::RETURN_TYPE BehaviourTree::Process()
{
	return m_root->Process();
}